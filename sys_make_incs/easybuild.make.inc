# System-dependent makefile options for ONCVPSP

# This version was contributed by Yann Pouillon via a patch.
# It might be incomplete.
# Needs "easybuild"
 
##### Edit the following lines to correspond to your compilers ####

F77        = gfortran
F90        = gfortran
CC         = gcc
FCCPP      = cpp

FLINKER     = $(F90)

FCCPPFLAGS = -ansi -DLIBXC_VERSION=203  #This probably should not be changed

##### Edit the following optimization flags for your system ####

FFLAGS     = -O3 -ffast-math -funroll-loops
CFLAGS     = -O3		

##### Edit the following LAPACK and BLAS library paths for your system ####

LIBS = -lopenblas

#
# The xmlf90 library (its wxml subsystem) is needed to generate XML.
# You can download xmlf90 from http://launchpad.net/xmlf90
# Follow the installation instructions in the package
#
XMLF90_ROOT=$(EBROOTXMLF90)
LIBS += -L$(XMLF90_ROOT)/lib -lxmlf90
INC += -I$(XMLF90_ROOT)/include

##### Edit the following for to use libxc if available #####

OBJS_LIBXC =	exc_libxc_stub.o

# oncvpsp is compatible with libxc
# To build oncvpsp with libxc, uncomment 3 of the following lines and edit
# the paths to point to your libxc library and include directories
# make clean in src before rebuilding after changing this

##for libxc 2.1.0 and later use
LIBS += -L$(EBROOTLIBXC)/lib -lxcf90 -lxc
FFLAGS += -I$(EBROOTLIBXC)/include
OBJS_LIBXC =	functionals.o exc_libxc.o 

