# Version without libxc support
# 
#
F77        = gfortran	#EDIT
F90        = gfortran	#EDIT
FCCPP      = cpp
CC         = gcc	#EDIT
RANLIB     = echo
AR         = ar

FLINKER     = $(F90)

FFLAGS     = -O3 -msse3 #EDIT
CFLAGS     = -O3		#EDIT

FCCPPFLAGS = -DLIBXC_VERSION=203  #This probably should not be changed
                                  #NOTE: libxc V4 does not yet work with this version
#
# Linear algebra libraries
#
#LIBS = -L/usr/local/opt/lapack/lib -llapack  -L/usr/local/opt/openblas/lib  -lopenblas	#EDIT
#
# This line works for MacOSX with veclibFort (a patch for Apple's veclib)
# (See https://github.com/mcg1969/vecLibFort)
#
#LIBS = -lvecLibFort
#
# For generality, define the LAPACK_LIBS symbol, either here or through a module system
## LAPACK_LIBS=
LIBS=$(LAPACK_LIBS)
#
# The xmlf90 library (its wxml subsystem) is needed to generate XML.
# You can download xmlf90 from http://gitlab.com/siesta-project/libraries/xmlf90
# Compile it and define the symbol XMLF90_ROOT, either here, or through a module system.
#
##XMLF90_ROOT=  ####  $(HOME)/lib/gfortran-7.2.0/xmlf90-1.5.4
#
LIBS += -L$(XMLF90_ROOT)/lib -lxmlf90
INC += -I$(XMLF90_ROOT)/include

#
# Optional LIBXC support

# oncvpsp is presently compatible with libxc-2.0.3, not later releases (???)

# To build oncvpsp with libxc, use the following lines and edit
# the paths to point to your libxc library and include directories
# make clean in src before rebuilding after changing this
#
# Define LIBXC_ROOT here or through a module system
##LIBXC_ROOT=   #### $(HOME)/lib/gfortran-7.2.0/libxc-2.2.3
#LIBS += -L$(LIBXC_ROOT)/lib -lxc -lxcf90
#INC += -I$(LIBXC_ROOT)/include
#OBJS_LIBXC =	functionals.o exc_libxc.o
#
# Otherwise, use only the following line
#
OBJS_LIBXC =	exc_libxc_stub.o
#
#-------------------------------------------

